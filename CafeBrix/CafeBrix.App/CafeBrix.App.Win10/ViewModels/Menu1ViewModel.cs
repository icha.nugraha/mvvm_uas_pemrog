﻿using CafeApp.Win10.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Printing.Interop;
using System.Windows.Input;
using CafeBrix.App.Win10.Models;
using System.Threading.Tasks;

namespace CafeBrix.App.Win10.ViewModels
{
    public class Menu1ViewModel : BaseViewModel {
        public Menu1ViewModel() {
           Makanans = new ObservableCollection<Makanan>();
            modelmenu = new Makanan();

        }
        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Makanan> Makanans {
            get
            {
                return makanans;
            }
            set
            {
                SetProperty(ref makanans, value);
            }
        }

        
        public Makanan ModelMakanans {
            get {
                return modelmenu;
            }
            set {
                SetProperty(ref modelmenu, value);
            }
        }
        public event Action OnReload;
        private ObservableCollection<Makanan> makanans;
        private Makanan modelmenu;

        }
    }

