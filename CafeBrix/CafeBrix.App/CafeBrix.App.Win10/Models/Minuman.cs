﻿namespace CafeBrix.App.Win10.Models
{
    public class Minuman
    {
        public int Uid { get; set; }
        public string Nama { get; set; }
        public string Description { get; set; }
        public uint Amount { get; set; }
        public int Cost { get; set; }
    }
}
