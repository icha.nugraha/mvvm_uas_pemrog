﻿namespace CafeBrix.App.Win10.Models
{
    public class Menu
    {
        public int Uid { get; set; }
        public string Nama { get; set; }
        public string Description { get; set; }
        public uint Amount { get; set; }
        public int Type { get; set; }
        public int price { get; set; }
        public Minuman Minuman { get; set; } 
        public Makanan Makanan { get; set; } 
        public Harga Harga { get; set; } 
    }
}
