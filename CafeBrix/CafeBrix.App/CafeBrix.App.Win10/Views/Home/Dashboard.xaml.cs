﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CafeBrix.App.Win10.Views.Menus;

namespace CafeBrix.App.Win10.Views.Home
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            NewMethod();

            void NewMethod() => InitializeComponent();
        }

        private void CoffeKlaten_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CoffeJogja_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void File_Click(object sender, RoutedEventArgs e)
        {

        }

        private void View_Click(object sender, RoutedEventArgs e)
        {

        }

        private void File_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void CoffeKlaten_DpiChanged(object sender, DpiChangedEventArgs e)
        {

        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonToCart_Click(object sender, RoutedEventArgs e)
        {

        }

        private void File_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void File_Click_3(object sender, RoutedEventArgs e)
        {

        }

        private void Edit_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {

        }

        private void A_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu1().ShowDialog();
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu2().ShowDialog();
        }

        private void C_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu3().ShowDialog();
        }

        private void D_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu4().ShowDialog();
        }

        private void E_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu5().ShowDialog();
        }

        private void F_Click(object sender, RoutedEventArgs e)
        {
            new Menus.Menu6().ShowDialog();
        }

        private void CafeBrixKlaten_Click(object sender, RoutedEventArgs e)
        {
            new Menus.BrixCafeKlaten().ShowDialog();
        }

        private void Grab_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void GoFoodOngkir_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Grab_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void CafeBrixJogja_Click(object sender, RoutedEventArgs e)
        {
            new Menus.BrixCafeJogja().ShowDialog();
        }
    }
}
