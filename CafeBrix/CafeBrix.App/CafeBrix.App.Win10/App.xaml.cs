﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CafeBrix.App.Win10.Views.Home;
using CafeBrix.App.Win10.Models;

namespace CafeBrix.App.Win10
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Dashboard View { get; set; }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            View = new Dashboard();
            View.Show();
        }
    }
}
